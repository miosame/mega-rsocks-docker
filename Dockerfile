FROM dorowu/ubuntu-desktop-lxde-vnc:latest
COPY ./files /root/files
COPY ./shortcuts/ /root/.local/share/applications/
RUN apt-get update && apt-get install -y ca-certificates wget curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_12.x  | bash -
RUN apt-get update && apt-get install -y default-jre sqlite3 qt5-default nodejs
RUN dpkg -i "/root/files/rsocks_2.1.4_amd64.deb"
RUN apt-get install -f
RUN npm install --unsafe-perm -g proxy-lists