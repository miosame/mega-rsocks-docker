# mega-rsocks-docker

Dockerized:
* megabasterd (mega downloader)
* rsocks (proxy checker)
* proxyfetcher (fetches http/https proxies)

All with desktop over vnc:

![image](image.png)

# Usage

1. install docker and docker-compose
2. git clone this repo `git clone https://gitlab.com/miosame/mega-rsocks-docker.git`
3. `cd mega-rsocks-docker` to change to the cloned directory
3. bring the stack up: `docker-compose up -d` - the first time it will build all the things necessary, next `up`s will be faster

After it built and started, you should be able to access it at `http://localhost:6080/`.
