#!/usr/bin/env bash
echo "Gathering proxy lists.. please wait.."
proxy-lists getProxies --protocols="http,https" --output-file="/root/files/proxies_fetched.txt" > /dev/null 2>&1
echo "Done - output to: /root/files/proxies_fetched.txt"
echo "Press Enter to exit.."
read